`default_nettype none

module gates (
    input wire clk,
    input wire in1,
    input wire in2,
    output logic led1,
    output logic led2
);
    logic in1_r, in2_r;

    always_ff @(posedge clk) begin
        in1_r <= in1;
        in2_r <= in2;

        led1 <= (~in1_r) & (~in2_r);
        led2 <= (~in1_r) | (~in2_r);
    end

endmodule

`default_nettype wire
