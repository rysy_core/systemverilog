vlib work

vlog gates.sv
vlog gates_tb.sv

vsim work.gates_tb

add wave -position end sim:/gates_tb/clk
add wave -position end sim:/gates_tb/in1
add wave -position end sim:/gates_tb/in2
add wave -position end sim:/gates_tb/dut/in1_r
add wave -position end sim:/gates_tb/dut/in2_r
add wave -position end sim:/gates_tb/dut/led1
add wave -position end sim:/gates_tb/dut/led2

run -all

wave zoom full
